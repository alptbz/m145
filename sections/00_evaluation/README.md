# M145 - Bewertungskriterien LB2 - LB5 <!-- omit in toc -->

Übersicht über alle Bewertungskriterien zu den einzelnen Themenbereichen. Bitte beachten Sie zusätzlich die Bedingungen im [Intro](../01_intro/README.md#laborbericht).

# Inhaltsverzeichnis <!-- omit in toc -->
- [Labor 1 - GNS3 Einführung (LB2)](#labor-1---gns3-einführung-lb2)
- [Labor 2 - VLAN (LB3)](#labor-2---vlan-lb3)
- [WLAN Hausaufgabe (LB4)](#wlan-hausaufgabe-lb4)
- [WLAN Labor (LB5)](#wlan-labor-lb5)
- [SNMP Labor (LB6)](#snmp-labor-lb6)

# Labor 1 - GNS3 Einführung (LB2)

| N°  | Kriterium                                                                                                                                   | Punkte     |
| ---- | ---------------------------------------------------------------------------------------------------------------------------------- | -------- |
| 1    | Laborbericht rechtzeitig als PDF or Repository Link abgegeben.                                                                     | 4 P  |
| 2    | Laborbericht enthält alle geforderten Abschnitte (Übungen, Persönliches Fazit, usw. ).                                                                                  | 2 P  |
| 3    | **Übung 1 - Statisches Routing**                                                                                                       |      |
| 3.01 | GNS3 Labor ist gemäss Vorgabe aufgebaut und dokumentiert.                                                                         | 2 P  |
| 3.01 | Ping von PC des Lernenden zum R1 funktioniert und ist dokumentiert.                                                                | 2 P  |
| 3.02 | Ping von PC des Lernenden zum R2 funktioniert und ist dokumentiert.                                                               | 2 P  |
| 3.03 | Alle ausgeführten Befehle sind für Dritte nachvollziehbar dokumentiert.                                                           | 2 P  |
| 3.04 | Aufgabenstellung, Ziele, Parameter (IP Adressen, usw.) und Quellen (verwendete Anleitungen) sind in der Dokumentation aufgeführt. | 2 P  |
| 4    | **Übung 2 - Bridging**                                                                                                                 |      |
| 4.01 | GNS3 Labor ist gemäss Vorgabe aufgebaut und dokumentiert.                                                                         | 2 P  |
| 4.02 | Alle benötigten IP Adressen und Subnetze sind auf den Geräten korrekt konfiguriert.                                                | 2 P  |
| 4.03 | Die beiden VPCS haben eine IP-Adresse und können R1 anpingen.                                                                     | 2 P  |
| 4.04 | Aufgabenstellung, Ziele, Parameter (IP Adressen, usw.) und Quellen (verwendete Anleitungen) sind in der Dokumentation aufgeführt. | 2 P  |
| 5    | Übersichtlich, gute Lesbarkeit, kein Ballast                                                                                      | 2 P  |
| 6    | Punkte für das Lösen der Zusatzübung (Cisco 3745)                                                                                  | 6 P  |
|      |                                                                                             **Max. Punkte** - Intro Labor | 32 P |


<br/><br/>

# Labor 2 - VLAN (LB3)
| N°   | Kriterium                                                                                                                                                             | Max  |
|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
| 1    | Laborbericht rechtzeitig als PDF or Repository Link abgegeben.                                                                                                        | 4 P  |
| 2    | Laborbericht enthält alle geforderten Abschnitte.                                                                                                                     | 2 P  |
| 3    | Übung 3 - VLAN                                                                                                                                                        |      |
| 3.01 | GNS3 Labor ist gemäss Vorgabe aufgebaut und dokumentiert.                                                                                                             | 4 P  |
| 3.02 | Drei VLANs mit den korrekten VLAN-IDs auf beiden Switches mit der "switch-chip-offloading"-Variante konfiguriert und dokumentiert.                                    | 4 P  |
| 3.03 | Für jeden Port tagged und untagged korrekt parametriert und dokumentiert.                                                                                             | 4 P  |
| 3.04 | Alle Ping Tests durchgeführt, Befehle dokumentiert und Ausgabe der pings mit Screenshots belegt.                                                                      | 4 P  |
| 3.05 | Wireshark-Analyse durchgeführt und mit Screenshots dokumentiert. Auf den Screenshots ist die VLAN-ID erkennbar markiert.                                              | 4 P  |
| 3.05 | Aufgabenstellung, Ziele, Parameter (IP Adressen, usw.), Konfigurationsexport aller Geräte und Quellen (verwendete Anleitungen) sind in der Dokumentation aufgeführt.  | 4 P  |
| 4    | Übung 4  - Praxisnahes VLAN Beispiel                                                                                                                                  |      |
| 4.01 | GNS3 Labor ist gemäss Vorgabe aufgebaut und dokumentiert. (bzw. Labor aus Übung 3 ist dupliziert)                                                                     | 2 P  |
| 4.02 | Auf R1 Für jedes VLAN ein Interface angelegt                                                                                                                          | 2 P  |
| 4.03 | Auf R1 für jedes VLAN ein Subnetz definiert gemäss Angaben und auf dem Router jeweils eine praxisübliche Hostaddresse konfiguriert  und dokumentiert.                 | 2 P  |
| 4.04 | Auf Switch 1 drei VLANs für die Verbindung /Trunk zu R1 konfiguriert und dokumentiert.                                                                                | 2 P  |
| 4.05 | Auf R1 für jedes der drei VLAN-interfaces einen DHCP Server konfiguriert und dokumentiert.                                                                            | 2 P  |
| 4.06 | Alle VPCS so angepasst, sodass diese ihre IP Adresse per DHCP beziehen.                                                                                               | 2 P  |
| 4.07 | Routing auf R1 so konfiguriert, dass die VPCS aus dem .23 Subnetz angepingt werden können.                                                                            | 2 P  |
| 4.08 | Aufgabenstellung, Ziele, Parameter (IP Adressen, usw.), Konfigurationsexport aller Geräte und Quellen (verwendete Anleitungen) sind in der Dokumentation aufgeführt.  | 2 P  |
| 5    | Übersichtlich, gute Lesbarkeit, kein Ballast                                                                                                                          | 2 P  |
|      | **Max. Punkte** - VLAN Labor | **48 P** |


# WLAN Hausaufgabe (LB4)
(Einzelbewertung!)

| **N°** | **Kriterium**                                                                                                                                                                                | **Max**  |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| 1      | Laborbericht rechtzeitig als PDF abgegeben.                                                                                                                                                  | 4 P      |
| 2      | Laborbericht enthält alle geforderten Abschnitte.                                                                                                                                            | 4 P      |
| 3      | Eigenen Kanal im Windows gefunden (GUI und oder Terminal)                                                                                                                                    | 2 P      |
| 4      | Screenshot 2.4 GHz Band (Grafische Darstellung Kanäle)                                                                                                                                       | 2 P      |
| 5      | Screenshot 5 GHz Band (Grafische Darstellung Kanäle)                                                                                                                                         | 2 P      |
| 6      | Frage "Sind die Netzwerke gut auf den verfügbaren Funkkanälen verteilt?" <br/>beantwortete (1P) und korrekt (1P)                                                                             | 2 P      |
| 7      | Frage "Welche Netzwerke (Namen) stören die anderen?"<br/>beantwortete (1P) und korrekt (1P)                                                                                                  | 2 P      |
| 8      | Frage "Welche Netzwerke verwenden zwei Kanäle?" beantwortete (1P) und korrekt (1P)                                                                                                           | 2 P      |
| 9      | Frage "Weshalb ist es besser mehrere WLAN-Netzwerke auf dem gleichen Funkkanal zu haben, anstatt auf sich<br/>überschneidenden Funkkanälen? Gibt es dieses Problem auch mit dem 5 GHz WLAN?" | 2 P      |
| 10     | Heatmap vorhanden (6P) und korrekt (6P)                                                                                                                                                      | 12 P     |
| 11     | Übersichtlich, gute Lesbarkeit, kein Ballast                                                                                                                                                 | 2 P      |
|        | **Total**                                                                                                                                                                                    | **36 P** |

# WLAN Labor (LB5)

| N°   | Kriterium                                                                                                               | Max. Punkte   |
|------|-------------------------------------------------------------------------------------------------------------------------|--------|
| 1    | Laborbericht rechtzeitig als PDF abgegeben mit allen geforderten Abschnitten.                                           | 4.0 P  |
| 2    | **Laborübung: Übung 4** WLAN                                                                                                |        |
| 2.01 | Konfiguration ist komplett im Anhang (Schriftart: Monospace)                                                            | 1.0 P  |
| 2.02 | Aufgabe 4: Fragen beantwortet (Hardware-Typ, Unterstützte Frequenzbänder, Anzahl physische Interfaces, Antennen-Typ)    | 1.0 P  |
| 2.03 | Aufgabe 6: Screenshots von Wireless-Scan im Mikrotik und mit Handy/PC App vorhanden. Inhalt ist plausibel und korrekt.  | 1.0 P  |
| 2.04 | Aufgabe 8: Frage zu Veteilung korrekt beantwortet.                                                                      | 1.0 P  |
| 2.05 | Aufgabe 9/10: Die Channels 1,6,11 für 2.4GHz korrekt konfiguriert.  (Alternativ: 5 GHz Channels)                        | 1.0 P  |
| 2.06 | Aufgabe 11/12/13: SSID, Passwort korrekt konfiguriert                                                                   | 1.0 P  |
| 2.07 | Aufgabe 14: Erfolgreiche Verbindung eines Clients mit dem AP dokumentiert                                               | 1.0 P  |
| 2.08 | Aufgabe 15: Screenshot der TX Geschwindigkeit (Entweder vom MikroTik UI oder Client UI).                                | 1.0 P  |
| 2.09 | Aufgaben 16/18/18: Versuch mit mehreren Kanälen erfolgreich durchgeführt und dokumentiert.                              | 1.0 P  |
| 2.10 | Aufgaben 19-23: Durchgeführt, Screenshots vorhanden und Erfolgreich                                                     | 1.0 P  |
| 3    | **WLAN-Planung: Aufgabe** 1                                                                                                 |        |
| 3.01 | Komplexität der Umsetzung ist technisch begründet.                                                                      | 1.0 P  |
| 3.02 | Die Technische Umsetzung ist korrekt, nachvollziehbar und stimmig.                                                      | 1.0 P  |
| 3.03 | Geräte und Abdeckung sichtbar.                                                                                          | 1.0 P  |
| 3.04 | Alle benötigten Geräte vorhanden, Hersteller nachvollziebar, Geräte sinnvoll gewählt.                                   | 1.0 P  |
| 3.05 | Konfigurationseinstellung ist komplett, sinnvoll und hat keine (oder wenige) Fehler.                                    | 1.0 P  |
| 3.06 | Formale Anforderungen (Ansprechende Gestaltung, Grafiken, illustionen, usw.)                                            | 1.0 P  |
| 4    | **WLAN-Planung: Aufgabe** 2                                                                                                 |        |
| 4.01 | Mindestens 2, maximum 4 Accesspoints sind ausserhalb des Gebäudes auf dem Grundrissplan eingezeichnet.                  | 1.5 P  |
| 4.02 | Die Accesspoints befinden sich an unterschiedlichen Seiten der Villa (Optimal: An gegenüberliegenden Ecken der Villa).  | 1.5 P  |
| 5    | Dokumentation: Übersichtlich, gute lesbar, kein Ballast                                                                 | 2.0 P  |
| 6    | Zusatzaufgabe Laborübung (Zugriff auf fremden Access Point) gelöst.                                                     | 2.0 P  |
|      | **Total**                                                                                                                   | **27.0 P** |

# SNMP Labor (LB6)

| N°   | Kriterium                                                                   | Max   |
|------|-----------------------------------------------------------------------------|-------|
| 1    | Vollständiger Laborbericht rechtzeitig als PDF abgegeben.                   |  4 P  |
| 2    | **GNS3 Übung - SNMP**                                                       |       |
| 2.01 | MIB Browser Übung durchgeführt, dokumentiert und alle Fragen beantwortet    |  4 P  |
| 2.02 | Zweckmässiges GNS3 Labor aufgesetzt und dokumentiert.                       |  4 P  |
| 2.03 | Methode zur Steigerung von "ifInUcastPkts.2" ist korrekt und dokumentiert.  |  4 P  |
| 3    | Übersichtlich, gute Lesbarkeit, kein Ballast                                |  4 P  |
|      | **Total**                                                                   | **20 P** |


# M145 - VLAN  <!-- omit in toc -->

 *Virtual Local Area Network*

 - Theoretische Grundlagen
 - Übung mit VLAN Simulator  
 - GNS 3 Laborübung

# Inhaltsverzeichnis  <!-- omit in toc -->
- [1. Einleitung](#1-einleitung)
- [2. Handlungsziele](#2-handlungsziele)
- [3. Bewertung](#3-bewertung)
- [4. Theoretische Grundlagen](#4-theoretische-grundlagen)
  - [4.1. Was ist ein VLAN?](#41-was-ist-ein-vlan)
  - [4.2. Warum VLAN?](#42-warum-vlan)
  - [4.3. Wie funktioniert ein VLAN?](#43-wie-funktioniert-ein-vlan)
    - [4.3.1. Portbasierte VLANs](#431-portbasierte-vlans)
    - [4.3.2. Tagged VLANs](#432-tagged-vlans)
  - [4.4. Aufbau Ethernet Frame](#44-aufbau-ethernet-frame)
  - [4.5. VLAN und Routing](#45-vlan-und-routing)
  - [4.6. Verständnisfragen](#46-verständnisfragen)
- [5. VLAN Simulator](#5-vlan-simulator)
- [6. Laborübung](#6-laborübung)
  - [6.1. Übung 3 – VLANs programmieren und beobachten](#61-übung-3--vlans-programmieren-und-beobachten)
    - [6.1.1. Lernziele](#611-lernziele)
    - [6.1.2. Aufgabe](#612-aufgabe)
    - [6.1.3. Paramter](#613-paramter)
    - [6.1.4. Ziele](#614-ziele)
    - [6.1.5. Hinweise](#615-hinweise)
  - [6.2. Übung 4 – Praxisnahes VLAN Beispiel mit Router und DHCP Server](#62-übung-4--praxisnahes-vlan-beispiel-mit-router-und-dhcp-server)
    - [6.2.1. Lernziele](#621-lernziele)
    - [6.2.2. Aufgabe](#622-aufgabe)
    - [6.2.3. Ziele](#623-ziele)
    - [6.2.4. Parameter](#624-parameter)
- [7. Anhang](#7-anhang)
  - [7.1. Befehlskatalog MikroTik](#71-befehlskatalog-mikrotik)
  - [7.2. Befehls-Beispiele MikroTik](#72-befehls-beispiele-mikrotik)
- [8. Ressourcen](#8-ressourcen)
- [9. Author](#9-author)

# 1. Einleitung

Im ersten Teil der Laborübungen haben wir die GNS3 Client Anwendung auf unserem Computer installiert, uns mit dem Server verbunden und die ersten
Einstiegsübungen zu den Wiederholungen der Module 117 und 129 gemacht.

In diesem Teil erarbeiten wir nun das Netzwerkprotokoll VLAN. 

 - Im ersten Teil lernen Sie die theoretischen Grundlagen für VLAN.
 - Im zweiten Teil üben Sie ihr Switching Verständnis mit dem VLAN Simulator.
 - Im dritten Teil führen Sie in Ihrer Gruppe die Laborübungen aus.

# 2. Handlungsziele

Netzwerke in virtuelle LAN aufteilen und konfigurieren.

Handlungsnotwendige Kenntnisse:

1.  Kennt die Möglichkeiten zur physikalischen und logischen Gliederung eines
    Netzwerkes und ihre Auswirkungen auf Performance und Verfügbarkeit.
2.  Kennt die VLAN Typen (tagbasiert, portbasiert und dynamisch) und typische
    Konfigurationsmöglichkeiten der Geräte.

# 3. Bewertung
Die Bewertungskriterien finden Sie [hier](../00_evaluation/README.md).



# 4. Theoretische Grundlagen

##  4.1. Was ist ein VLAN?

Wie lief es bisher…

Wenn Sie ein Netzwerk aufbauen wollen, kaufen Sie sich einen Switch und bauen so das Netzwerk auf.

Wollen Sie ein weiteres Netzwerk, kaufen Sie einen weiteren Switch und bauen das nächste Netzwerk auf. Jetzt haben Sie zwei voneinander unabhängige Netzwerke.

Das gleiche Prinzip benutzen VLANs – aber eben virtuell…

Ein grosses Netzwerk wird in unabhängige kleinere Netzwerke zerlegt. Um dabei Hardware-Kosten zu sparen, wird das aber nicht physikalisch wie im obigen Beispiel gemacht, sondern virtuell innerhalb vom Switch (oder den Switches).

Virtuelle Netzwerke laufen auf der gleichen Hardware, sind aber untereinander abgeschottet. Dadurch können Sie in einem grossen System viele kleinere Systeme aufbauen. So lassen sich zum Beispiel Hierarchien und Zugriffe viel besser kontrollieren. Aber auch die Sicherheit wird deutlich verbessert. Sensible Daten werden so vor neugierigen Blicken geschützt.

##  4.2. Warum VLAN?

-   Strikte Trennung: VLANs trennen (segmentieren) Netzwerke auf Layer 2
    komplett. Systeme zwischen verschiedenen VLANs «sehen» und stören sich nicht und können keine (unerwünschten) Daten austauschen, wie z.B. unerwünschte Broadcasts.
-   Flexibilität bei der Zuordnung von Endgeräten zu Netzwerksegmenten,
    unabhängig vom physischen Standort des Geräts, also logische Gruppen
    innerhalb der physikalischen Topologie.
-   Priorisierung von bestimmtem Datenverkehr wie z.B. VoIP.
-   Mehr Dienste im Netz (Drucker, IP-Telefonie, IP-TV, etc.) ziehen eine
    grössere Komplexität nach sich. Diese Komplexität kann man reduzieren, indem man konsequent für jeden Dienst ein eigenes VLAN im Netz bezeichnet.
-   Die Sicherheit ist bei VLANs besser als bei rein geswitchten Netzen, die durch Angriffsmöglichkeiten wie MAC-Spoofing und MAC-Flooding unsicher sind.
-   Sicherheitsrelevante Systeme können in ein separates VLAN gelegt und durch entsprechende Massnahmen besser geschützt werden.
-   Zur Verbindung von VLANs kommen Router (Layer 3 Switches) zum Einsatz, die gegen Layer-2-Attacken unempfindlich sind. Zusätzlich bietet Routing die Möglichkeit, Firewalls auf Layer-3-Basis einzusetzen.

##  4.3. Wie funktioniert ein VLAN?

Die Zuordnung der Teilnetzwerke auf dem Switch (oder den Switches) kann auf zwei Arten erfolgen, als Portbasierte VLANs (Untagged) der als Tagged VLANs.

###  4.3.1. Portbasierte VLANs

Mit portbasierten VLANs unterteilen Sie **einen einzelnen physischen Switch** einfach **auf mehrere logische** Switches (VLANs). Dabei wird jedem Port ein VLAN zugewiesen.

![https://www.thomas-krenn.com/de/wikiDE/images/7/72/VLAN-Grundlagen-Beispiel-1.png](media/0508d59fb241a51d0958c4b0db255602.png)

Im folgenden Beispiel teilen wir einen physischen 8-Port Switch (Switch A) auf zwei logische Switches auf:

| **Switch A**    |             |                           |
|-----------------|-------------|---------------------------|
| **Switch-Port** | **VLAN ID** | **angeschlossenes Gerät** |
| 1               | 1 (grün)    | PC A-1                    |
| 2               |             | PC A-2                    |
| 3               |             | -                         |
| 4               |             | -                         |
| 5               | 2 (orange)  | PC A-5                    |
| 6               |             | PC A-6                    |
| 7               |             | -                         |
| 8               |             | -                         |

Obwohl alle PCs an einem physischen Switch angeschlossen sind, können aufgrund der VLAN Konfiguration nur folgende PCs jeweils miteinander kommunizieren:

-   PC A-1 mit PC A-2
-   PC A-5 mit PC A-6

Nehmen wir an, dass im Nachbarraum ebenfalls vier PCs stehen. Nun sollen PC B-1 und PC B-2 mit PC A-1 und PC A-2 im ersten Raum kommunizieren können. Ebenfalls soll die Kommunikation zwischen PC B-5 und PC B-6 aus Raum 2 mit PC A-5 und PC A-6 im Raum 1 möglich sein.

Im Raum 2 haben wir wieder einen Switch:

| **Switch B**    |             |                           |
|-----------------|-------------|---------------------------|
| **Switch-Port** | **VLAN ID** | **angeschlossenes Gerät** |
| 1               | 1 (grün)    | PC B-1                    |
| 2               |             | PC B-2                    |
| 3               |             | -                         |
| 4               |             | -                         |
| 5               | 2 (orange)  | PC B-5                    |
| 6               |             | PC B-6                    |
| 7               |             | -                         |
| 8               |             | -                         |

Damit die beiden VLANs hier verbunden werden können, benötigen wir bei portbasierten VLANs **zwei** Kabel, je eines für die entsprechenden VLANs:

-   von Switch A Port 4 zu Switch B Port 4 (für das VLAN 1)
-   von Switch A Port 8 zu Switch B Port 8 (für das VLAN 2)

![https://www.thomas-krenn.com/de/wikiDE/images/d/d0/VLAN-Grundlagen-Beispiel-2.png](media/f02c889dfee798c1029bc8c2d1d9dc63.png)

*Portbasiertes VLAN mit zwei Switches.*

###  4.3.2. Tagged VLANs

Bei **Tagged VLANs** können mehrere VLANs über einen **einzelnen Switch-Port** genutzt werden. Die einzelnen Ethernet Frames bekommen dabei ein **Tag** hinzugefügt, in dem die VLAN-ID vermerkt ist, zu dessen VLAN das Frame gehört.  

Wenn im gezeigten Beispiel beide (!) Switches tagged VLANs beherrschen, kann damit die gegenseitige Verbindung mit **einem einzelnen Kabel** erfolgen. Diese eine Verbindung mit den tagged VLANs wird **Trunk** genannt:

![https://www.thomas-krenn.com/de/wikiDE/images/4/4f/VLAN-Grundlagen-Beispiel-3.png](media/f5d10c8325e93bb26226bbe78c0e87bd.png)

In der Norm IEEE 802.1q ist definiert, wie die Tags in das Ethernet-Frame
eingefügt werden.

Zusatzinfo: Die Ports werden zwischen **Access-Ports** und **Trunk-Ports** unterschieden.  
Das **Access-Port** dient dazu, **ein** Gerät mit dem Switch zu verbinden, über diese Port wird **nur ein** zugewiesenes VLAN geführt.  
Ein Trunk-Port, ist in der Lage, mehrere VLANS zu transportieren, z.B. zwischen Switches oder zu einem Gerät, das mehrere VLANs bedient wie z.B. Server oder WLAN-APs.

##  4.4. Aufbau Ethernet Frame

Der VLAN Tag kommt in einem Ethernet Frame nach den MAC Adressen. Es enthält u.a. die VLAN-ID, die das Frame dem VLAN zuordnen:

![Ethernet-Frame-VLAN-Tag.png](media/e16203e87f249855bb426a25c4ec101b.png)

Bei tagged VLANs nach IEEE 802.1q werden die Pakete **vom Endgerät** (z. B. Tagging-fähigem Server oder Access-Point) **versehen**. Wenn das einspeisende Gerät selber kein tagging kennt, kann das der Switch für das Gerät übernehmen. 
Zu diesem Zweck wird dem Port eine **PVID** (Port VLAN ID) zugewiesen. Jedes **untagged Frame** das an diesem Port eingeht, wird mit dem entsprechenden VLAN-Tag (PVID) versehen und wird damit zu einem **tagged Frame.**

Achtung: Ein Frame mit Tags wird i.d.R. von einem Gerät, dass Tags nicht kennt als ungültig betrachtet und verworfen.

##  4.5. VLAN und Routing

VLANs arbeiten auf Layer 2 und sind damit von der IP-Adressierung grundsätzlich nicht betroffen, aber…

![Bildergebnis für layer 3 switch
symbol](media/78981e241deccc686ec1784cd5f74dd0.jpeg)

VLANs führen zu einer kompletten Trennung auf Layer 2. Das heisst aber auch, dass zwischen den VLANs kein Datenverkehr möglich ist. Wenn doch zwischen den VLANs Datenverkehr nötig ist, muss auf **Layer 3 geroutet** werden. Damit das aber geht ist es zwingend nötig, dass **jedes VLAN** **ein eigenes (Sub) IP-Netz bildet**. Diese IP-Netze werden dann über Router verbunden. Damit die Router die getagten Frames unterscheiden können (z.B. auf einem Trunk), müssen diese Router **tagging verstehen**. Diese Geräte werden allgemein als **Layer 3 Switches** oder **Routing Switches** bezeichnet.

##  4.6. Verständnisfragen

Beantworten Sie die nachfolgenden Fragen im Forms (Einzelaufgabe):

Den Link erhalten Sie von der Lehrperson.

1.  Auf welchem ISO-OSI-Layer arbeitet ein normaler Switch?
2.  Kann ich mehrere Subnetze über einen normalen, nicht VLAN-fähigen Switch
    führen?
3.  Nennen Sie Gründe, ein physikalisches LAN in virtuelle Netze aufzuteilen
4.  Darf ein VLAN Netzwerk als «Sicher» eingestuft werden?
5.  Was versteht man unter «Portbasiertem VLAN»?
6.  Was versteht man unter «Tagged VLAN»?

# 5. VLAN Simulator

Laden Sie auf <https://github.com/muqiuq/vlansimulator/releases> die aktuelle
Version des VLAN Simulators herunter und üben Sie damit Ihr Verständnis für die
innere Funktionsweise eines VLAN fähigen Switches.

# 6. Laborübung

##  6.1. Übung 3 – VLANs programmieren und beobachten

Als klassische VLAN Aufgabe wird gerne die Aufteilung eines Netzwerkes für
verschiedene Abteilungen verwendet. Jede Abteilung erhält dafür ein eigenes
VLAN. Ihre Aufgabe besteht nun darin, dass Netzwerk in GNS3 aufzubauen, zu
konfigurieren und Messungen mit Wireshark durchzuführen.

###  6.1.1. Lernziele

-   Sie sind in der Lage auf MikroTik Routern VLANs zu programmieren.
-   Sie können VLAN Tags mit Wireshark auslesen.

###  6.1.2. Aufgabe

![](media/71892d7ce4f069a3a2118f86641a05f4.png)

*Screenshot GNS3: So sollte ihr Labor aussehen.*

1.  Bauen Sie das Netzwerk wie auf *Abbildung 3* dargestellt in GNS3 nach.
2.  Verwenden Sie für SW1 und SW2 das Gerät «MikroTik CHF 6.47», für MGMT
    «Ethernet switch» und für die PCs «VPCS».
3.  Ändern Sie die Router Icons von SW1 und SW2 in Switch Icons ab.
4.  Definieren Sie für jede Abteilung eine VLAN Nummer (VLAN-ID). Nutzen Sie die
    Tabelle unten zur Festlegung der Nummern.
5.  Definieren Sie für jedes Gerät eine IP-Adresse aus dem **gleichen Subnetz.**
    **In der Praxis ist das unüblich**. Jedoch hilft es uns in dieser Übung
    Tests durchzuführen, um sicherzustellen, dass die Abteilungen nicht
    untereinander kommunizieren können. Nutzen für die Festlegung der Subnetze
    die Tabelle unten.
6.  Programmieren Sie die VLANs auf den beiden Switchen (Siehe Befehle im
    Anhang).
7.  Programmieren Sie die IP-Adressen auf den sechs PCs.
8.  Führen Sie Tests durch, die folgende Resultate erwarten:
    1.  PC1 kann PC4 anpingen (und umgekehrt).
    2.  PC2 kann PC5 anpingen (und umgekehrt).
    3.  PC3 kann PC6 anpingen (und umgekehrt).
    4.  PC1 kann PC2 **nicht** anpingen (und umgekehrt).
    5.  PC2 kann PC4 **nicht** anpingen (und umgekehrt).
    6.  PC6 kann PC2 **nicht** anpingen (und umgekehrt).
9.  Führen Sie auf dem Trunk (Verbindung zwischen SW1 und SW2) einen «package
    capture» mit Wireshark durch und analysieren die einzelnen Ethernet Frames.
    Finden Sie den VLAN Tag ihrer Pings?

###  6.1.3. Paramter

| Subnetz           | VLAN ID |
| - |  - |
| VLAN Buchhaltung  | Verwenden Sie Ihre Gruppennummer \* 100 + 1. *Beispiel Gruppennummer 5  5\*100 + 1 = 501*                             |
| VLAN Entwicklung  | Verwenden Sie Ihre Gruppennummer \* 100 + 2.* Beispiel Gruppennummer 5  5\*100 + 2 = 502*                             |
| VLAN Verkauf      | Verwenden Sie Ihre Gruppennummer \* 100 + 3.* Beispiel Gruppennummer 5  5\*100 + 3 = 503*                             |

###  6.1.4. Ziele

-   **Auf beiden Switches sind die drei VLANs konfiguriert.**
-   **Die beiden Switches sind über eine Trunk Leitung miteinander verbunden.**
-   **Alle oben aufgeführten Pings können ausgeführt werden.** (Führen Sie je einen Screenshot eines Pings der funktionieren sollte und eines Pings der nicht funktionieren darf in Ihrem Bericht auf.)
-   **VLAN Tag mit Wireshark für alle drei VLANs nachgewiesen.** (Führen Sie für jedes VLAN einen Screenshot wie in *Abbildung 4* in Ihrem Laborbericht auf.)

###  6.1.5. Hinweise

-   Der MGMT Switch und die Verbindung zu den beiden Switches dient lediglich dazu, dass Sie mit ihrem Webbrowser auf die jeweiligen Webkonfiguration zugreifen können.
-   Die VPCS vergessen ihre Konfiguration, wenn Sie diese ausschalten. Speichern Sie die Konfiguration mit dem Befehl «save».
-   Es gibt auf MikroTik mehrere Varianten VLANs zu verwenden. In dieser Übung ist die Variante zu verwenden, bei der die VLAN Funktion auf den *switch chip* ausgelagert wird (siehe [Ressourcen](#ressourcen)).

![](media/cfbacadbfdc9b7d42493f062cbf99fb8.png)

*Mit Wireshark können Sie die Ethernet Frames analysieren und finden dort den VLAN Tag.*

##  6.2. Übung 4 – Praxisnahes VLAN Beispiel mit Router und DHCP Server

In der Praxis erhält jedes VLAN ein eigenes Subnetz (Grundregeln: Ein Subnetz
pro VLAN), ein Default Gateway, einen DHCP Server. In dieser Aufgabe erweitern
Sie Ihr Netzwerk aus Übung 3 mit diesen Komponenten.

###  6.2.1. Lernziele

-   Sie können einen DHCP-Server konfigurieren.
-   Sie können einen DNS-Server konfigurieren.
-   Sie können einen Router für mehrere Subnetze konfigurieren.

###  6.2.2. Aufgabe

![](media/19211e333cd0956e76a4cd8f1435bcb1.png)

*Screenshot GNS3: Wie Übung 3 mit zusätzlichem Router R1.*

1.  Duplizieren Sie ihr Labor aus Übung 3.
2.  Passen Sie Ihr Labor, wie in obigen Abbildung dargestellt, an.
3.  Definieren Sie für jedes VLAN ein Subnetz. Nutzen Sie dafür die Tabelle unten für die Festlegung der Subnetze.
4.  Konfigurieren Sie auf dem Router die drei VLAN Interfaces. Nutzen Sie dafür keine Bridge, sondern erstellen Sie das VLAN interface direkt auf dem physischen interface. Das nachfolgende **Beispiel** zeigt einen Befehl, der auf dem physischen interface *ether2* das virtuelle VLAN interface für die VLAN ID *10* hinzufügt.
```
/interface vlan add interface=ether2 vlan-id=10 name=ether2_vlan10
```
5.  Geben Sie jedem VLAN interface eine praxisübliche Hostadresse.
6.  Passen Sie SW1 an und fügen Sie ether2 als Tagged Port für die drei VLANs hinzu.
```
/interface bridge vlan edit number=X tagged
```
8.  Fügen Sie für jedes interface (z.B. ether2_vlan10) einen DHCP Server hinzu. 
```
/ip dhcp-server setup
```
9.  Passen Sie Ihre VPCS so an, dass diese eine IP-Adresse via DHCP beziehen. *Tipp: Im Kontextmenü des VPCS können Sie «Edit config» wählen. Falls vorhanden löschen Sie die Zeile, die eine statische IP-Adresse zuweist und schreiben Sie stattdessen «dhcp» auf eine leere Zeile. Mit «Reload» lädt der VPCS die Konfigurationsdatei neu und macht DHCP.*
10. Prüfen Sie auf dem Router, ob alle VPCS eine IP Adresse bezogen haben.  
```
/ip dhcp-server lease print
```
11. Pingen Sie von einem VPCS den Router an.
12. Pingen Sie von einem VPCS einen VPCS aus einem anderen VLAN an. Dies sollte nun funktionieren. Weshalb? Beantworten Sie diese Frage mithilfe eines «package captures» mit Wireshark.
13. In Übung 1 haben Sie statisches Routing geübt. Versuchen Sie einen VPCS von Ihrem PC aus anzupingen.

###  6.2.3. Ziele

-   Alle VPCS erhalten via DHCP eine IP-Adresse vom Router **R1**.
-   Die VPCS können den Router R1 anpingen.
-   Jede Abteilung hat ein eigenes /24 Subnetz.

###  6.2.4. Parameter

| **Subnetz A** | **192.168.X.0/24** | *Verwenden Sie für X Ihre Gruppennummer plus 50. Beispiel: Gruppennummer 5 + 50 = 55  192.168.55.0/24* |
|---------------|---------------------------------------------------------------------------------------------------------------------------| ------ |
| **Subnetz B** | **192.168.X.0/24** | *Verwenden Sie für X Ihre Gruppennummer plus 50. Beispiel: Gruppennummer 5 + 60 = 65  192.168.65.0/24* |
| **Subnetz C** | **192.168.X.0/24** | *Verwenden Sie für X Ihre Gruppennummer plus 50. Beispiel: Gruppennummer 5 + 70 = 75  192.168.75.0/24* |

# 7. Anhang

##  7.1. Befehlskatalog MikroTik

Eckige Klammern «[]» werden als Platzhalter verwendet.

Neue VLAN fähige Bridge hinzufügen
```
/interface bridge add name=[bridgename] protocol-mode=none vlan-filtering=yes
```

Einen Port (Only Untagged) zu einer Bridge hinzufügen (z.B. für ein Endgerät)
```
/interface bridge port add bridge=[bridgename] comment="VLAN [Nummer] – [Name Abteilung]" frame-types=admit-only-untagged-and-priority-tagged hw=no interface=[interface] pvid=[VLAN Nummer]
```

Einen Port (Only Tagged) zu einer Bridge hinzufügen (z.B. als Trunk)

```
/interface bridge port add bridge=bridge1 frame-types=admit-only-vlan-tagged hw=no interface=ether8
```
Ein VLAN einer Bridge hinzufügen, sowie «tagged» und «untagged» ports definieren. 
```
/interface bridge vlan add bridge=[bridgename] comment="VLAN [Nummer]" tagged=[interfaces kommagetrennt] untagged=[intefaces kommagetrennt] vlan-ids=[VLAN Nummer]
```
Den Hostnamen setzen:
```
/system identity set name=[Name]
```

DHCP Server konfigurieren
```
/ip dhcp-server lease print
```

Gesamte Konfiguration exportieren
```
/export
```

Gesamte Konfiguration in ein File exportieren (kann über die WebGui heruntergeladen werden)
```
/export file=filename
```

##  7.2. Befehls-Beispiele MikroTik

Der nachfolgende Befehl fügt der bridge1 die Ports ether4 und ether5 untagged und den Port ether8 tagged für das VLAN 10 hinzu:

```
/interface bridge vlan** add bridge=*bridge1* comment=*"VLAN 10"*
tagged=*ether8* untagged=*ether4,ether5* vlan-ids=*10*
```

Der nachfolgende Befehl setzt den Namen des Gerätes auf «SW1»:
```
/system identity set name=SW1
```

# 8. Ressourcen
 - [Manual:Basic VLAN Switching](https://wiki.mikrotik.com/wiki/Manual:Basic_VLAN_switching)
 - [https://wiki.mikrotik.com/wiki/Manual:Switch_Chip_Features](https://wiki.mikrotik.com/wiki/Manual:Switch_Chip_Features)

# 9. Author

Philipp Albrecht, 02.06.2021
# WLAN Labor <!-- omit in toc -->

In der letzten Übung haben Sie sich wie theoretischen Grundlagen zum Thema WLAN erarbeitet. In diesem Teil werden wir uns nun mit der Konfiguration eines Access Points und der Projektierung von WLAN-Anlagen beschäftigen.

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Handlungsziele](#1-handlungsziele)
- [2. Laborübung](#2-laborübung)
  - [2.1. GNS Übung 5 – WLAN](#21-gns-übung-5--wlan)
    - [2.1.1. Lernziele](#211-lernziele)
    - [2.1.2. Aufgabe](#212-aufgabe)
- [3. WLAN-Planung](#3-wlan-planung)
  - [3.1. Aufgabe 1: Gasthof «Zum Welahn»](#31-aufgabe-1-gasthof-zum-welahn)
    - [3.1.1. Erwartetes Resultat](#311-erwartetes-resultat)
    - [3.1.2. Aufgabe](#312-aufgabe)
    - [3.1.3. Inhalt ihrer Planung](#313-inhalt-ihrer-planung)
  - [3.2. Aufgabe 2: WLAN im Aussenbereich](#32-aufgabe-2-wlan-im-aussenbereich)
    - [3.2.1. Aufgabe](#321-aufgabe)
- [4. Anhang](#4-anhang)
  - [4.1. Beispiel MikroTik Befehle](#41-beispiel-mikrotik-befehle)

# 1. Handlungsziele 

Netzwerke mit Wireless LAN erweitern und mit gesichertem Zugang konfigurieren.

Handlungsnotwendige Kenntnisse:

1.  Kennt WLAN Standards, Antennenarten und typische Einsatzgebiete. sowie deren Vorteile und Nachteile
2.  Kennt WLAN-Sicherheitsmassnahmen

# 2. Laborübung

**Wichtig:**

-   Der Laborbericht **MUSS** im Anhang die **komplette Konfiguration** einer **Monospace** Schriftart enthalten. Verwenden Sie den Befehl /export file=teamname im Terminal um die gesamte Konfiguration in eine Datei zu exportieren. Sie können diese nach unter *Files* herunterladen.
-   Beantworten in Ihrem Laborbericht Sie alle unten aufgeführten Fragen in der Aufgabenstellung

## 2.1. GNS Übung 5 – WLAN

Jede Gruppe erhält von der Lehrperson ein MikroTik Gerät. Arbeiten Sie die einzelnen Punkte der Aufgabe durch und dokumentieren Sie ihre Ergebnisse. **MikroTik Befehl-Beispiele** finden Sie im Anhang.

### 2.1.1. Lernziele

-   Sie können ein sicheres WLAN auf einem WLAN fähigen MikroTik Router konfigurieren
-   Sie können mehrere SSIDs auf einem physischen WLAN interface aufschalten.
-   Sie können den richtigen Kanal für Ihren Access Point konfigurieren.

### 2.1.2. Aufgabe

1.  Holen Sie bei der Lehrperson einen Access Point ab.
2.  Packen Sie den Access Point **vorsichtig** aus und schliessen Sie in mit dem vorhandenen Netzteil an das Stromnetz an.
3.  Verbinden Sie mit einem Ethernet-Kabel einen Laptop der Gruppe mit dem Access Point, geben Sie sich eine statische IP Adresse aus dem Subnet *172.16.92.0/24* (Gruppe 1 verwendet 172.16.92.101/24, Gruppe 172.16.92.102/24, usw.) und rufen das Webinterface des Gerätes auf (Die IP Adresse des MikroTik Gerätes ist auf dem Gehäuse angebracht).
4.  Versuchen Sie folgende Fragen zu Ihrem Gerät zu beantworten:
    1.  Welcher Hardware-Typ hat ihr WLAN interface (Marke + Modellnummer)?
    2.  Welche Frequenzbänder unterstützt(en) ihr(e) WLAN interface(s)?
    3.  Wie viele physischen WLAN interfaces sind auf ihrem Gerät vorhanden?
    4.  Was für ein Typ Antenne besitzt ihr Gerät?
5.  Aktivieren Sie ihr WLAN interface.
6.  Scannen Sie mithilfe der Funktion *Scan* des WLAN interfaces das 2.4 GHz Frequenzband ab. Welche Kanäle sind belegt? Verwenden Sie einen WLAN Kanalscanner auf einem Handy oder Laptop. Stellen Sie unterschiede fest? ( Screenshots von beiden Scans in Ihren Laborbericht einfügen).
7.  Wenn Ihr Gerät 5GHz fähig ist, machen Sie das gleiche für dieses Frequenzband.
8.  Gibt es hier an der TBZ Access Points, die sich nicht an die optimale Aufteilung auf die Kanäle 1,6,11 halten?
9.  Fügen Sie unter /interface wireless channels für das 2.4GHz Band die drei Kanäle 1,6 und 11 in eine Channel-Liste mit dem Namen *«2.4GHz Channels»* ein.   
    (Lassen Sie diesen Schritt aus, wenn das Gerät kein 2.4GHz WLAN interface besitzt.)
10. Fügen Sie unter /interface wireless channels für das 5GHz Band drei **nicht** **DFS** Kanäle in eine Channel-Liste mit dem namen *«5GHz Channels»* ein.   
    (Lassen Sie diesen Schritt aus, wenn das Gerät kein 5GHz WLAN interface besitzt.)
11. Setzen im *Default Security Profile* die Passphrase auf EnteEnteEnte und aktivieren Sie **nur** WPA2.   
    Beantworten Sie in Ihrem Laborbericht die Frage, weshalb Sie **WPA** nicht aktivieren sollten.
12. Setzen Sie die SSID. Verwenden Sie dazu folgende Formel:  
     «Gruppe» + Nummer.  
    Bsp. «Gruppe5»  
    Nach dem obligaten Teil dürfen Sie natürlich einen beliebigen Freitext einfügen.
13. Wählen Sie für Ihr 2.4 GHz WLAN interface einen vorkonfigurieren *Channel* aus. Koordinieren Sie an der Wandtafel den Kanal. Ziel ist es, dass nicht alle Gruppen den gleichen Kanal verwenden.
14. Versuchen Sie sich mit Ihrem Access Point zu verbinden.   
    Sehen Sie die SSID?   
    Erhalten Sie eine IP-Adresse?  
     Dokumentieren Sie Ihre Erkenntnisse im Laborbericht.
15. Prüfen Sie mit welcher TX Geschwindigkeit Sie mit dem Access Point verbunden sind. ( Screenshot!)
16. Beantworten Sie folgende Frage in Ihrem Laborbericht:  
    Wie viele Kanäle verwenden Sie? ( Laborbericht)
17. Versuchen Sie den Access Point so zu konfigurieren, dass Sie zwei Kanäle verwenden.
18. Prüfen Sie nochmals Ihre TX Geschwindigkeit.   
    Was stellen Sie fest? Notieren Sie Ihre Feststellungen mit Screenshots in Ihrem Laborbericht.
19. Fügen Sie ein tagged VLAN interface (VLAN-ID 145) auf einem freien Ethernet Port hinzu.
20. Erstellen Sie ein virtuelles WLAN interface mit der SSID wie oben, aber fügen Sie zu beginn noch «Client» ein.
21. Erstellen Sie eine virtuelle Bridge und fügen Sie beiden interfaces der vorhergehenden Schritte dieser hinzu.
22. Verbinden Sie Ihren Access Point mit dem zentralen Switch.
23. Verbinden Sie sich nun mit einem Laptop mit diesem neuen WLAN.  
    Erhalten Sie eine IP-Adresse?  
    Können Sie sich mit dem Internet verbinden?  
    ( Notieren im Laborbericht inkl. Screenshots)
24. Wenn Sie nun eine IP Adresse erhalten haben und Zugriff auf das Internet haben sind Sie Fertig .

**Zusatzaufgabe**: Versuchen Sie über Ihren Access Point auf die Konfiguration einer anderen Gruppe zuzugreifen. Hinterlassen Sie bei einem beliebigen interface einen **netten** *Kommentar.* Haben Sie auch Kommentare erhalten? Was müssten Sie machen, um einen solchen Zugriff zu verhindern? Wie lautet der Befehl im MikroTik Router?  
(Wenn Sie ein Passwort setzen, nehmen Sie bitte tbz123456)

# 3. WLAN-Planung

Fügen Sie die Lösung der nachfolgenden Aufgaben in Ihren Laborbericht ein.

## 3.1. Aufgabe 1: Gasthof «Zum Welahn»

Der Gasthof «Zum Welahn» soll neu mit WLAN ausgerüstet werden. Die Gäste des Restaurants sollen mit ihren WLAN-fähigen Notebooks, iPads und Smartphones auf Internet und Mails zugreifen können.

![http://www.teklounge.ch/wp-content/uploads/M145/WLAN/Welahn.jpg](media/fead10d584e9cd5c811c0c1033fafc0a.jpeg)

Abbildung 1: Grundriss des Gasthofes "Welahn"

### 3.1.1. Erwartetes Resultat

Die WLAN-Abdeckung soll möglichst optimal erfolgen. D.h. die Positionen der Access-Points (AP), sowie Antennentypen sollen so gewählt werden, dass an den genannten Orten der Funkempfang optimal ist und gleichzeitig vermieden wird, dass «fremde Ohren» Gelegenheit erhalten, mitzuhören. Die Konfigurationen der AP sollen für einen optimalen Betrieb ausgelegt sein.

Es sollen folgende Bereiche erschlossen werden:

-   Das Funkenstübli
-   Die gedeckte und geschlossene Veranda (Wintergarten)
-   Die Terrasse (offene Gartenterrasse)

### 3.1.2. Aufgabe

Erstellen Sie ein Konzept für die WLAN-Ausrüstung für den Gasthof «Zum Welahn» mit folgenden Teilaufgaben:

-   Bestimmen Sie die optimalsten Standorte für die Access-Points und zeichnen sie diese auf dem Gebäudegrundriss ein
-   Bestimmen Sie die für diese Aufgabe am besten geeigneten Antennentypen
-   Evaluieren Sie die entsprechenden Geräte und erstellen Sie eine Materialliste mit Kostenübersicht
-   Definieren Sie alle notwendigen Konfigurationseinstellungen für die Access-Point’s

### 3.1.3. Inhalt ihrer Planung

 - Die Umsetzung ist technisch begründet
 - **Grundrissplan:** Geräte und Abdeckung sichtbar
 - **Materialliste:** Alle benötigte Geräte vorhanden, Hersteller nachvollziehbar, Geräte sinnvoll gewählt 
 - **Konfigurationseinstellung:** Komplett, sinnvoll, keine Fehler
 - **Formale Anforderungen:** Formal ansprechende Gestaltung; Grafiken, Illustrationen Fusszeile/Kopfzeile, Seitenzahl, Name, …
 - Zusätzliche Überlegungen



## 3.2. Aufgabe 2: WLAN im Aussenbereich

Für eine Villa soll ein WLAN im Aussenbereich projektiert werden. Dafür hat ein Techniker mit mobile Access Points Messungen vor Ort gemacht. Der Typ der Access Points ist bereits festgelegt: [UniFi FlexHD](https://store.ui.com/collections/unifi-network-access-points/products/unifi-flexhd).

### 3.2.1. Aufgabe

-   Analysieren Sie die beiden Messergebnisse.
-   Bestimmen Sie die Anzahl und die Standorte der Access Points.
-   Zeichen Sie die Access Points auf dem Grundrissplan ein.

![](media/bfdf8cde87f4e4e64f89e4b5b8edee5d.png)

Abbildung 2: Messergebnis des mobilen Access Points "2SKYSTAR"

![](media/7399ed5167e3a4c99f1aae6ae076462e.png)

Abbildung 3: Messergebnis des mobilen Access Points "1SKYSTAR"

![](media/3e3823c34cfffb8e51a2ab30b9777ba6.png)

Abbildung 4: Grundriss der Villa.

# 4. Anhang

## 4.1. Beispiel MikroTik Befehle

Füge die Frequenz 2412 MHz in die Kanalliste mit dem Namen ch1 ein:

```
/interface wireless channels add band=2ghz-b/g/n frequency=2412 list=2GHz width=20 name=ch1
```
<hr>
Füge die Frequenz 2412 MHz in die Kanalliste mit dem Namen ch1 ein mit einem «extension-channel»:

```
/interface wireless channels add band=2ghz-b/g/n extension-channel=Ce frequency=2412 list=2GHz name=ch1-6 width=20                                                                            
```
<hr>
Konfiguriere das *WLAN1* interface als *AP Bridge* für die Verwendung in der Schweiz.

```
/interface wireless set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no frequency-mode=regulatory-domain country=switzerland mode=ap-bridge name=wlan1 ssid=MYWLAN wps-mode=disabled
```
<hr>
Konfiguriert das default *security-profile* für die Verwendung mit WPA2 und einem *Pre-Shared-Key*

```
/interface wireless security-profiles set [ find default=yes ] authentication-types=wpa2-psk mode=dynamic-keys supplicant-identity=MikroTik wpa2-pre-shared-key=passphrase
```
<hr>
Fügt dem physischen interface *ether3* das VLAN123 als tagged interface hinzu.

```
/interface vlan add name=ether3-vlan123 vlan-id=123 interface=ether3-admin
```
<hr>
Fügt der bridge *bridge123* das VLAN interface *ether3-vlan123* hinzu.

```
/interface bridge port add bridge=bridge123 interface=ether3-vlan123
```
<hr>
Fügt ein virtuelles WLAN interface *wlan2*  dem physischen interface *wlan1* mit der SSID *asd* hinzu.                                                                              

```
/interface wireless add disabled=no master-interface=wlan1 name=wlan2 ssid=asd wps-mode=disabled
```

# Mein WLAN Zuhause <!-- omit in toc -->

*Wireless Local Area Network*

Theoretische Grundlagen
WLAN-Messungen

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Einleitung](#1-einleitung)
- [2. Handlungsziele](#2-handlungsziele)
- [3. Theoretische Grundlagen](#3-theoretische-grundlagen)
- [4. Praktische Übungen](#4-praktische-übungen)
  - [4.1. Übung 1: Standard Informationen zu der WLAN-Verbindung von Windows](#41-übung-1-standard-informationen-zu-der-wlan-verbindung-von-windows)
  - [4.2. Übung 2: Kanalbelegung](#42-übung-2-kanalbelegung)
    - [4.2.1. Ziel](#421-ziel)
  - [4.3. Übung 3: Heatmap](#43-übung-3-heatmap)
    - [4.3.1. Vorgehen](#431-vorgehen)

# 1. Einleitung

WLAN ist heutzutage aus der Netzwerktechnik nicht mehr wegzudenken. Auch wenn LTE mit 5G bereits sehr hohe Datenraten erzielt, ist WLAN ein fixer Bestandteil eines Home und Business-Netzwerkes. Zudem benötigt es im Vergleich zu LTE wesentlich weniger Prozessorleistung. Fast jedes User Endgerät unterstützt heutzutage WLAN.

In diesem Skript erarbeiten Sie sich die theoretischen Grundlagen zu WLAN und führen danach Messungen in diesem eigenen Zuhause aus.

![BlkBox BB-E01P - The World's Smallest ESP8285-Based WiFi Module -  Electronics-Lab.com](media/af2d378754bd09515789697ced11bd1b.jpeg)

*Dieser winzige Chip unterstützt WLAN.*

# 2. Handlungsziele

Netzwerke mit Wireless LAN erweitern und mit gesichertem Zugang konfigurieren.

Handlungsnotwendige Kenntnisse:

1.  Kennt WLAN Standards, Antennenarten und typische Einsatzgebiete. sowie deren Vorteile und Nachteile
2.  Kennt WLAN-Sicherheitsmassnahmen

# 3. Theoretische Grundlagen

**Lesen** Sie den Skriptausschnitt «**compendio_145_2015_kap6_funknetzwerke**» und **beantworten** Sie anschliessend die **Verständnisfragen**. Den Link zum Formular erhalten Sie von der Lehrperson. 

Verwenden Sie zusätzlich als Quellen für die Beantwortung der Fragen:

-   <http://www.elektronik-kompendium.de/sites/net/index.htm>
-   <https://de.wikipedia.org/wiki/Wireless_Local_Area_Network>
-   <https://en.wikipedia.org/wiki/List_of_WLAN_channels>
-   <https://www.bakom.admin.ch/dam/bakom/de/dokumente/bakom/telekommunikation/Technologie/WLAN/factsheet_wlan.pdf.download.pdf/factsheet_wlan.pdf>
-   <https://www.krackattacks.com/>

# 4. Praktische Übungen

Die nachfolgenden Übungen sind eine Einzelarbeit. Erstellen Sie einen Laborbericht und fügen Sie Screenshots ein, die zeigen, wie Sie das Ziel erreicht haben.

Vergessen Sie nicht die allgemeinen Fragen (siehe GNS3 Labor Einführung) in Ihrem Laborbericht zu beantworten und denken Sie daran, dass Ihr Bericht für Fachpersonen nachvollziehbar sein muss.

##  4.1. Übung 1: Standard Informationen zu der WLAN-Verbindung von Windows

1.  Versuchen Sie mit Windows Standardprogramme (GUI basiert) herauszufinden was für einen Kanal Sie haben und welche TX Geschwindigkeit.
2.  Versuchen Sie Informationen zu Ihrer aktuellen WLAN-Verbindung über einen PowerShell Befehl auszulesen.  
    *Tipp: Der Befehl beginnt mit «netsh»*

Machen Sie von beiden Ansichten einen Screenshot und fügen Sie diese in Ihren Laborbericht ein.

![](media/a5fc2f3cb177fba5e3b064ff2870a662.png)

Abbildung 2: Informationen zu der aktuellen WLAN-Verbindung in Windows 10.

##  4.2. Übung 2: Kanalbelegung

Analysieren Sie mit einem beliebigen WiFi Analyse Programm die WLAN-Netzwerke in Ihrer Umgebung.

| ![Wifi Analyzer Android app helps you identify optimal channels for your  wireless router to improve speed – The Gadgeteer](media/6e03aec27dd05a985b8159501c911589.png) | ![WLAN Empfang mit Android App "Wifi Analyzer" optimieren](media/a14f4d0504e18d889ccccd6d4bede0fe.png)    |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
| Schlecht: Die Netzwerke überschneiden sich.                                                                                                                             | **Gut: Die Netzwerke sind auf die Standard Kanäle 1, 6 und 11 aufgeteilt und überschneiden sich nicht.**  |

Vorschläge für WiFi Analyse Tools:

-   <https://www.microsoft.com/de-ch/p/wifi-analyzer/9nblggh33n0n?activetab=pivot:overviewtab>
-   <https://play.google.com/store/apps/details?id=abdelrahman.wifianalyzerpro&hl=de_CH&gl=US>

### 4.2.1. Ziel

-   Erstellen Sie einen Screenshot von Ihrer Analyse des 2.4 GHz und 5 GHz Bandes.
-   Beantworten Sie dazu die folgenden Fragen **zu Ihrer Analyse** in ihrem Laborbericht:
    -   Sind die Netzwerke gut auf den verfügbaren Funkkanälen verteilt?
    -   Welche Netzwerke (Namen) stören die anderen?
    -   Welche Netzwerke verwenden zwei Kanäle?
-   Beantworten Sie diese allgemeine Fragen in Ihrem Laborbericht:
    -   Weshalb ist es besser mehrere WLAN-Netzwerke auf dem gleichen Funkkanal zu haben, anstatt auf sich überschneidenden Funkkanälen? Gibt es dieses Problem auch mit dem 5 GHz WLAN?

##  4.3. Übung 3: Heatmap

Das Ziel ist es, dass Sie eine Heatmap von Ihrer Wohnung / Haus erstellen. Dafür finden Sie im Internet zahlreiche Tools.

Die Wahl des Tools steht Ihnen frei. Das Ziel ist es eine Heatmap Grafik Ihrer Räumlichkeiten zu erstellen:

![](media/29c08334120b4e88c43a12e08220e406.png)

Abbildung 3: WLAN Heatmap mit wenigen Messpunkten.

![Wi-Fi Heatmap Software - Visualize Coverage and Capacity \| Ekahau](media/bb10e4168b59957f164c80b7c7dc8f9b.png)

Abbildung 4: Umfangreiche Heatmap mit vielen Messpunkten (Quelle: https://www.ekahau.com/solutions/wi-fi-heatmaps/)

Tools (Auswahl):

-   Open source WiFi Surveyor <https://github.com/ecoAPM/WiFiSurveyor>
-   Solarwindows Wi-Fi Heat Maps <https://www.solarwinds.com/network-performance-monitor/use-cases/wifi-heat-map>
-   Ekahau Heatmapper 1.1.4 <https://tbzedu-my.sharepoint.com/:u:/g/personal/philipp_albrecht_tbz_ch/Ee2qj3yFvxBGiXnd7o8WA6IBUaQIByjE8DeQingzBiskDw?e=7uLtoL>

###  4.3.1. Vorgehen

1.  Suchen Sie sich ein Heatmap Programm für Ihren PC oder Handy im Internet aus oder nehmen Sie Programm aus den Vorschlägen. **Sehr empfehlenswert ist der Ekahau Heatmapper**, da er versucht die Position der Access Points herauszufinden.
2.  Erstellen Sie einen groben Plan Ihrer Wohnung / Hauses (von Hand oder mit einen Zeichenprogramm (Visio, Draw.io, Inkscape) und importieren Sie diesen in die gewählte Applikation.
3.  Laufen Sie mit Ihrem Laptop / Mobiltelefon durch Ihre Räumlichkeiten und führen Sie Messungen durch (z.B. in jedem Raum an zwei Orten).
4.  Fügen Sie Ihre Heatmaps in Ihren Laborbericht ein.

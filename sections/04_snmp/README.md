# SNMP <!-- omit in toc -->

*Simple Network Management Protocol*

 - Theoretische Grundlagen
 - GNS3 Übungen

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Einleitung](#1-einleitung)
- [2. Theorie](#2-theorie)
- [3. Praktische Übungen](#3-praktische-übungen)
  - [3.1. MIB Browser](#31-mib-browser)
    - [3.1.1. Lernziele](#311-lernziele)
    - [3.1.2. Hinweise](#312-hinweise)
- [4. Einleitung zur GNS3 Übung 5](#4-einleitung-zur-gns3-übung-5)
- [5. GNS3 Labor – Übung 5](#5-gns3-labor--übung-5)
  - [5.1. Einleitung](#51-einleitung)
  - [5.2. Übersicht](#52-übersicht)
  - [5.3. Lernziele](#53-lernziele)
  - [5.4. Aufgabe](#54-aufgabe)
  - [5.5. Hinweise](#55-hinweise)
  - [5.6. OpenVPN](#56-openvpn)
  - [5.7. GNS3 Labor Konfiguration](#57-gns3-labor-konfiguration)
    - [5.7.1. MikroTik Router](#571-mikrotik-router)
  - [5.8. Links](#58-links)
  - [5.9. Screenshots der Musterlösung](#59-screenshots-der-musterlösung)
  - [5.10. Zusatzaufgaben](#510-zusatzaufgaben)
- [6. Author](#6-author)

# 1. Einleitung

Während in Privatbereich der Ausstieg des Routers meist erst bemerkt wird, wenn das Netflix Video nicht mehr lädt und die Konfiguration des eigenen einzelnen Access Points noch über die GUI erledigt werden kann (wenn nicht nur ein Knopfdruck notwendig ist), haben Netzwerke im Business höhere Anforderungen. Dabei sind zwei Aspekte besonders wichtig: Die Überwachung und die Verwaltung aller Netzwerkkomponenten. Wenn bei über 100 Switchen einer eine Störung hat, soll nicht gewartet werden, bis dieser einen Totalausfall hat. Die frühzeitige Erkennung von Störungen ist für einen Reibungslosen Netzwerkbetrieb essenziell.

Nebst vielen proprietären Lösungen (Cisco Meraki, Cisco Business Dashboard, UniFi Controller, usw.) die meist eigene Protokolle verwenden bzw. diese dem Administrator verborgen werden, gibt es das weit unterstützte SNMP.

# 2. Theorie

Lesen Sie die beiden Dokumente **compendio_145_2012_kap4_instrumente_zur_ueberwachung.pdf** und **NW_Grundl_06_Kap_15_netzwerkueberwachung.pdf** durch und beantworten Sie anschliessend die Fragen im Forms (Einzelarbeit). Den Link erhalten Sie von der Lehrperson. 

# 3. Praktische Übungen

## 3.1. MIB Browser

![](media/1f2a911a13f281e90fe0bcca984a7fcd.png)

Abbildung 1: iReasoning MIB Browser, SNMP Walk eines MikroTik Cloud Routers

![](media/34956f1d5096708af79b50d36459f5c4.png)

Abbildung 2: Verwendetes Demo-Labor.

Diese Übung hat **keine Schritt-für-Schritt Anleitung**. Ihr Ziel ist es eine volle «Result Table» wie in Abbildung 1 zu bekommen. Sobald Sie das geschafft haben, schauen Sie sich in den MIBs herum.

Beantworten Sie folgenden Fragen mithilfe von Screenshots in Ihrem Laborbericht:

-   Finden Sie die «ipAdEntAddr»? Was hat es für einen Wert? Ist der Zugriff Nur-Lesen oder Lesen-Schreiben?
-   Wie können Sie den Wert «ifInUcastPkts.2» steigern? Wo finden Sie diesen Wert in der MikroTik WebGui?

### 3.1.1. Lernziele

-   Sie können mit einem SNMP (MIB) Browser einen Netzwerkgerät (Agent) auslesen.
-   Sie erarbeiten sich die Schritte zu Ihrem Ziel selbstständig Mithilfe von Hinweisen.

### 3.1.2. Hinweise

-   Laden Sie sich auf <http://www.ireasoning.com/download.shtml> den iReasoning MIB Browser Free Personal Edition herunter und installieren Sie diesen auf Ihrem Computer.
-   Sie können jedes Labor verwenden.
-   Im MikroTik CHR müssen Sie SNMP für die community public zuerst aktivieren.


# 4. Einleitung zur GNS3 Übung 5


# 5. GNS3 Labor – Übung 5

## 5.1. Einleitung

In den vorhergehenden Übungen haben Sie sich die theoretischen Grundlagen zu SNMP erarbeitet und mit dem MIB Browser experimentiert. Sie kennen nun wichtigsten Aspekte von SNMP und können mit den Abkürzungen wie OID und MIB etwas anfangen.

Die meisten Netzwerk Monitoring Software und Netzwerkkomponenten unterstützen SNMP. [PRTG](https://de.wikipedia.org/wiki/PRTG_Network_Monitor), [Zabbix](https://de.wikipedia.org/wiki/Zabbix), [Icinga](https://de.wikipedia.org/wiki/Icinga), [Nagios](../Nagios) gehören zu einer Auswahl von bekannten Produkten. Dazu kommen immer mehr Cloud Lösungen wie [Azure Monitor](https://azure.microsoft.com/de-de/services/monitor/) oder [LogicMonitor](https://www.logicmonitor.com/), die vor allem für die Überwachung von modernen Serverinfrastrukturen ausgelegt sind. Wenn Sie [network monitoring ranking](https://www.google.com/search?q=network+monitoring+ranking) googlen werden Sie noch viel mehr Lösungen finden. Einige dieser Monitoring Software bieten auch einen zusätzlichen «Client» an, der auf den zu überwachenden Endgeräten installiert wird, dort Metriken sammelt und zum Server übermittelt.

In dieser Übung werden Sie sich mit **einer** Netzwerk Monitoring Software Ihrer Wahl auseinandersetzen.

## 5.2. Übersicht

![](media/56ca8914e4019c0fde2fe9cc7a35bb14.png)

*Abbildung 1: Schematische Darstellung der Übung*

**Pro Gruppe** installieren Sie jeweils eine Netzwerk Monitoring Software in **einer virtuellen Maschine auf Ihrem eigenen Gerät** und verbinden diese via VPN mit dem GNS3 Labor.

Abschliessend stellt jede Gruppe der Klasse Ihre Netzwerkmonitoringsoftware vor. Die Gruppen sollten unterschiedliche Netzwerk Monitoring Software verwenden. Koordinieren Sie mit den anderen Klassen. 

Unten finden Sie zu den verschiedenen Komponenten, die Sie im Laufe der Übung konfigurieren müssen, Tipps und Befehle.

**Wichtig: Das ist KEINE Step-By-Step Anleitung!** Überlegen wie Sie vorgehen möchten und welche Informationen Sie benötigen (**I**PERKA Informieren), Prüfen Sie welche Informationen bereits in diesem Skript vorhanden sind, Planen (I**P**ERKA) Sie ihr vorgehen.

## 5.3. Lernziele

-   Sie trainieren Ihr methodisches Arbeiten in Bezug auf die Installation von Serverapplikationen.
-   Sie lernen eine OpenVPN Verbindung von einem Linux Client einzurichten.
-   Sie lernen eine Netzwerk Monitoring Software zu installieren und Netzwerkgeräte einzubinden.

## 5.4. Aufgabe

-   Wählen Sie **zwei** verschiedene Netzwerk Monitoring Software zum Testen aus. ( Recherche im Internet)  
    Ich empfehle Ihnen [checkmk](https://checkmk.com/de) und [PRTG](https://www.paessler.com/prtg/download) . Wenn Sie andere Software testen möchten, sprechen Sie das mit der Lehrperson ab.
-   Teilen Sie die Software unter den Gruppenmitgliedern auf: Jedes Mitglied soll maximal 1 Software bei sich installieren.
-   Setzen Sie eine virtuelle Maschine auf (Cloud Instanz, Vagrant, usw. auch gut)
-   Verbinden Sie Ihre virtuelle Maschine per OpenVPN mit ihrem GNS3 Labor.
-   Richten Sie sich pro Gruppe ein GNS3 Labor ein.
-   Installieren Sie pro VM maximal eine Monitoring Software.
-   Fügen Sie mindestens zwei Netzwerkgeräte ihrer Monitoring Software hinzu.
-   Testen Sie Ihre Netzwerk Monitoring Software. Leitfragen:
    -   Welche Informationen kann ich in der Netzwerk Monitoring Software sehen?
    -   Wie schnell werden Änderungen in der Netzwerkmonitoring Software angezeigt?
    -   Was passiert, wenn ich ein überwachter Host ausschalte?
    -   Wie kann ich eine Benachrichtigung absetzen, wenn ein Gerät nicht erreicht werden kann?
-   Beantworten Sie die Fragen indem Sie Ihre Netzwerk Monitoring Software entsprechend konfigurieren und dies in Ihrem Laborbericht dokumentieren.

## 5.5. Hinweise 

Nebst den üblichen Richtlinien für den Laborbericht beachten Sie insbesondere folgendes:

-   Führen Sie verwendete Links Installationsanleitungen und Tutorials auf
-   Zeigen Sie in Ihrer Dokumentation wie Sie die Netzwerk Monitoring Software getestet haben (Siehe Abbildung 4, Abbildung 6, Abbildung 7 usw.)

Bitte behalten Sie die VM zur Beurteilung auf Ihrem Computer bis zum Ende des Moduls.

## 5.6. OpenVPN

Damit Sie von Ihrer virtuellen Maschine auf das GNS3 Projekt zugreifen können, müssen Sie diese mit VPN einbinden. Die nachfolgenden Befehle müssen Sie in einem Terminal ausführen. Alle Befehle sind für Ubuntu 20.04 ausgelegt.

1.  Installieren Sie openvpn  
    sudo apt install openvpn
2.  Kopieren Sie ihre ovpn Datei in das Verzeichnis /etc/openvpn. Der Dateinamen sollte auf «.conf» enden.
3.  Bearbeiten Sie ihre Konfigurationsdatei und fügen Sie am Ende die folgenden Zeilen ein:  
``` 
script-security 2     
ifconfig 192.168.23.51 255.255.255.0  
log-append /var/log/openvpn-gns3.log
```    
4.  Laden Sie den init-Prozess neu:
```bash
 $ systemctl daemon-reload
```
5.  Starten Sie den openvpn service  
```bash
 $ systemctl start openvpn
```
6.  Prüfen Sie ob die Verbindung funktioniert mithilfe der folgenden Befehle:  
```bash
 $ ping 192.168.23.1  
 $ ip address  
 $ sudo tail -n 60 /var/log/openvpn-gns3.log
```

## 5.7. GNS3 Labor Konfiguration

### 5.7.1. MikroTik Router

Es ist empfehlenswert folgende Konfigurationen auf Ihren MikroTik Routern im GNS3 Projekt vorzunehmen

DHCP Client löschen:
``` 
/ip dhcp-client remove numbers=0
```
Statische IP Adresse zuweisen aus dem Management Netzwerk zuweisen  
```
    /ip address add interface=ether1 address=192.168.23.X/24
```
Für die beiden Client PCs:
Bridge anlegen:
```
/interface bridge add name=bridge1
```

Der Bridge eine IP Adresse zuweisen:
```
/ip address add interface=bridge1 address=192.168.X.1/24
```

Konfigurieren Sie einen DHCP Server:
```
/ip dhcp-server setup
```

SNMP aktivieren:
```
/snmp set enabled=yes
```

## 5.8. Links

-   [https://docs.checkmk.com/latest/en/intro_finetune.html\#switchports](https://docs.checkmk.com/latest/en/intro_finetune.html#switchports)
-   Windows 10 Installation ISO (Sie brauchen keinen Lizenzschlüssel):
-   <https://software-download.microsoft.com/db/Win10_20H2_v2_EnglishInternational_x64.iso?t=2b9863f8-b193-4ed9-a349-da898aebf6a2&e=1617715250&h=982f7d3ae0ba93e5b40acd1934bb97f2>
-   https://www.vagrantup.com/

## 5.9. Screenshots der Musterlösung

![](media/ea232ce4faf6c681bc49fb3806ca3e50.png)

*Abbildung 2: GNS3 Labor mit zwei MikroTik Routern*

![](media/7148fa14534095947187a8bc48aefcfc.png)

*Abbildung 3: checkmk Dashboard mit zwei überwachten Hosts*

![](media/8f45e987bd66684f82902ed1eff6adf0.png)

*Abbildung 4: checkmk: Zwei Hosts sind konfiguriert und erreichbar*

![](media/4051f1d361942d660911e435271b317b.png)

*Abbildung 5: Laborkonfiguration vereinfacht: Die IP Adressen auf den VPCS müssen nicht manuell konfiguriert werden.*

![](media/f85ba4bf3d5408e33a8aaa8c36eb6fbe.png)

*Abbildung 6: Mithilfe der VPCS Traffic generieren.*

![](media/33ec07ab4d5283de2e9148462c312ae9.png)

*Abbildung 7: Der generierte Traffic ist im checkmk sichtbar.*

![](media/6ed0449eefba357cf6cedccdfa6581ed.png)

*Abbildung 8: In checkmk müssen die Konfigurationsänderungen immer "Aktiviert" bzw. übernommen werden.*

![](media/eb93a06c565b55047e1d3e355026df36.png)

*Abbildung 9: Im checkmk Dashboard wird eine Fehlermeldung angezeigt: Host «R1» ist «DOWN».*

![](media/ba0c46632e52352f20172c9fd62ec1cd.png)

*Abbildung 10: Router 1 Übersicht im PRTG WebGUI.*

## 5.10. Zusatzaufgaben

1.  checkmk und PRTG fügen nur die aktiven Interfaces des MikroTik Gerätes hinzu. Welche Einstellungen müssen Sie vornehmen, damit auch die inaktiven interfaces angezeigt werden?
2.  Konfigurieren Sie eine Regel, die immer eine Notification / Meldung auslöst, wenn ein gewisser Wert überschritten wird (z.B. verwendete Bandbreite) und lösen Sie diese Regel durch eine entsprechende Versuchsanordnung aus.


# 6. Author
Philipp Albrecht, 
02.06.2021
# M145 - GNS3 Lab - Einführung <!-- omit in toc -->

Im Rahmen des Moduls 145 werden Sie verschiedene neue Netzwerktechnologien kennenlernen. Die dazugehörigen praktischen Übungen werden Sie (fast) alle mit GNS3 (Graphical Network Simulator-3) durchführen. GNS3 ist ein leistungsstarker und umfangreicher Netzwerk Emulator, der es erlaubt echte Networking-Software auszuführen. Vielleicht haben Sie bereits mit VMWare, VirtualBox oder Hyper-V gearbeitet. Diese Produkte erlauben es Ihnen Betriebssysteme wie Microsoft Windows, Linux oder sogar Mac OS X in einer virtuellen Maschine auf ihrem Computer auszuführen. GNS3 lässt Sie dasselbe tun mit Cisco IOS, Juniper Routers, MikroTik und einigen weiteren Herstellern. Zusätzlich hat es ein grafisches Frontend mit dem Sie die emulierte Netzwerktopologie erstellen und visualisieren können.

Die Leistungsfähigkeit von GNS3 zeigt sich dadurch, dass es u.a. an Hochschulen und Netzwerk-Zertifizierungskurse (z.B. CCNA) eingesetzt wird. Es erlaubt Ihnen auch komplexe Routing, VPN Szenarien oder sogar Systeme mit OpenFlow zu simulieren. Wenn zum ersten Mal mit einer Netzwerktechnologie (z.B. OSPF) arbeiten und Ihr Konzept überprüfen wollen, dann ist GNS3 genau das richtige Tool.

![](media/1d36f9957c84bab58e0427f5e16c075d.png)

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Laborbericht <a name="laborbericht"></a>](#1-laborbericht-)
  - [1.1. Bewertung](#11-bewertung)
- [2. GNS3](#2-gns3)
  - [2.1. Projektverwaltung](#21-projektverwaltung)
  - [2.2. Beschaffung von Informationen](#22-beschaffung-von-informationen)
- [3. Installation](#3-installation)
  - [3.1. GNS3 Client](#31-gns3-client)
  - [3.2. OpenVPN](#32-openvpn)
  - [3.3. Verbindungstest](#33-verbindungstest)
  - [3.4. GNS3 GUI](#34-gns3-gui)
- [4. Übung 1 – Statisches Routing](#4-übung-1--statisches-routing)
  - [4.1. Lernziele](#41-lernziele)
  - [4.2. Aufgabe](#42-aufgabe)
  - [4.3. Hilfreiche Befehle und Tipps](#43-hilfreiche-befehle-und-tipps)
- [5. Übung 2 – Bridging](#5-übung-2--bridging)
  - [5.1. Lernziele](#51-lernziele)
  - [5.2. Aufgabe](#52-aufgabe)
  - [5.3. Ressourcen](#53-ressourcen)
  - [5.4. Tipps](#54-tipps)
- [6. Zusatzübung – Cisco 3745 (Freiwillig)](#6-zusatzübung--cisco-3745-freiwillig)
  - [6.1. Lernziele](#61-lernziele)
  - [6.2. Aufgabe](#62-aufgabe)
  - [6.3. Ressourcen](#63-ressourcen)
- [7. Anhang](#7-anhang)
  - [7.1. Tipps für den Laborbericht im Word/LibreOffice (A4 Printformat)](#71-tipps-für-den-laborbericht-im-wordlibreoffice-a4-printformat)
  - [7.2. Tipps für den Laborbericht in Markdown](#72-tipps-für-den-laborbericht-in-markdown)

# 1. Laborbericht <a name="laborbericht"></a>

Während des ganzen Moduls erstellen Sie für jede praktische Übung einen Laborbericht (pro Gruppe) mit **mindestens** folgendem Inhalt:

-   Titelseite, Inhaltsverzeichnis, Datum, Modulname, Klassen, Gruppennummer, Gruppenmitglieder, Seitenzahl

-   Name der Übung(en)
-   Protokollieren Sie jeden ausgeführte Befehl, sofern er zum Ziel führte. Entweder als Text oder, wenn Sie die GUI eines Netzwerkgerätes verwenden, als Screenshot.
-   Vollständige Konfigurationsdatei für jedes Netzwerkgerät
-   Verwendete Quellen (Links, Videos, usw.)
-   Hilfestellungen, die Sie von Lernenden anderer Teams erhalten oder Sie geleistet haben.
-   Ein Abschnitt in dem **jedes** Gruppenmitglied **einzeln** folgende Fragen in einem kurzen Text (**max**. eine halbe A4 Seite) beantwortet:
    -   Was habe ich Neues gelernt? Was kannte ich bereits?
    -   Wo hatte ich Schwierigkeiten? Was nehme ich für die Zukunft mit?

Sofern Sie von der Lehrperson keine andere Anweisung erhalten haben, ist der Laborbericht als **PDF jeweils bis Ende Woche (Sonntag) abzugeben.** Sie dürfen **Übungen zum gleichen Thema** in einem **einzigen** **Laborbericht** zusammenfassen. Die Laborberichte werden bewertet und sind **Teil Ihrer Modulnote**. Der Laborbericht kann auf Deutsch oder Englisch verfasst werden. Fügen Sie jeweils als Erfolgsnachweis **Screenshots** ein (z.B. Screenshot mit GNS3 Schema und offener Konsole mit erfolgreichen pings).  
Weitere Tipps zur Strukturierung Ihres Berichtes finden Sie in Abschnitt *7. Anhang.*

**Alternative Tools:  
Es ist es erlaubt Ihren Laborbericht in einem anderen Tool (z.B. Markdown/Latex auf GitHub, notion.so, usw.) zu erstellen. Sprechen Sie das mit der Lehrperson ab und vereinbaren Sie individuelle Bewertungskriterien!**

## 1.1. Bewertung

| **Kriterium**                                                                                                                                                                                   | **Max. Anzahl Punkte**             |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| Laborbericht **rechtzeitig** als PDF abgegeben.                                                                                                                                                 | 4 P.                               |
| Laborbericht enthält alle geforderten Abschnitte.                                                                                                                                               | 2 P.                               |
| **Jeweils pro Übung:** Der dokumentierte Lösungsweg im Laborbericht funktioniert. Beweis für das Erreichen des Zieles erbracht (Screenshot(s)).                                                 | 4 P. *(Alle Übungen zusammen)*     |
| Übersichtlich, gute Lesbarkeit, **kein Ballast**                                                                                                                                                | 2 P.                               |
| Extrapunkte für besonders herausragende Berichte (Schöne Formulierungen, sinnvolle zusätzliche Versuche, übersichtlich dargestellte Zeichnungen von Netzwerktopologien, Gelöste Zusatzaufgaben) | 4 P.                               |
| Total                                                                                                                                                                                           | 16 P.                              |
| *Formel für die Berechnung der Note*                                                                                                                                                            | *[Erreichte Punkte] / 16 \* 5 + 1* |

Für jedes Kriterium erhalten Sie Punkte. Daraus errechnet sich Ihre Note. Kopierte Berichte (Plagiat) oder sachfremde Berichte geben automatisch die Note eins (1). Laborberichte die mehr als vier Tage verspätet abgegeben wurden, erhalten auch die Note (1). **Beachten** Sie **abweichende Bewertungskriterien** in den Übungsbeschreibungen.

**Ihr Ziel ist es den Bericht so zu gestalten, dass Sie ihn in Zukunft als Anleitung verwenden können. Weiter muss die Lehrperson mithilfe des Berichtes beurteilen können, ob Sie die notwendigen Handlungskompetenzen für das Modul erreicht haben. Lesen Sie an Bericht vor der Abgabe durch und fragen Sie sich: «Verstehe ich das auch noch in 10 Jahren?» und «Könnte eine fremde Fachperson mithilfe dieses Dokumentes das Labor ohne Rückfragen nachbauen?» Wenn Sie beide Fragen mit Ja beantworten können, dann haben Sie das Ziel erreicht.**

# 2. GNS3

## 2.1. Projektverwaltung

Für die Projektverwaltung innerhalb von GNS3 gilt es folgendes zu beachten:

-   Erstellen Sie für jede Übung ein eigenes GNS3 Projekt.

-   Geben Sie dem Projekt einen sinnvollen Namen (z.B. Gruppennummer und Übungsnummer).

-   Entladen Sie Projekte, die Sie gerade nicht verwenden.

## 2.2. Beschaffung von Informationen

Es wird von Ihnen erwartet, dass Sie selbstständig im Internet und in den Unterlagen nach Informationen suchen. Wenn Sie nicht weiterkommen und alle Rechercheversuche erfolglos waren, **zögern Sie nicht und fragen Sie die Lehrperson**.

# 3. Installation

Da GNS3 jedes einzelne Netzwerkgerät emuliert, beziehungsweise eine virtuelle Maschine startet, benötigt das viel Leistung. Deshalb steht für jede Gruppe ein dezidierter GNS3 Server bereit. Beachten Sie dabei folgendes:

-   Der Server hat nur begrenzt Leistung.

-   Starten Sie **nicht** alle Netzwerkgeräte auf einmal, um die Festplatte nicht auszulasten.

-   Emulieren Sie **nicht** mehr als 6 bis 7 Netzwerkgeräte pro Projekt.

-   Achten Sie auf die Auslastungsangaben im GNS3.

Natürlich dürfen Sie sich zusätzlich einen eigenen GNS3 Server auf Ihrem Laptop oder einem anderen Server aufsetzen. In der [GNS3 Dokumentation](https://docs.gns3.com/) finden Sie dafür reichlich Anleitungen. Für die Gruppenübungen empfehle ich Ihnen, den zur Verfügung gestellten Server zu verwenden.

## 3.1. GNS3 Client

Führen Sie die nachfolgenden Schritte aus, um auf Ihrem Computer den GNS3 Client zu installieren.

| 1 | Laden Sie auf *github* den GNS3 Installer herunter.  <https://github.com/GNS3/gns3-gui/releases>  **Wichtig: Nehmen Sie die Version 2.2.21 !**                               | ![](media/d7b2b1e2e87020f8ce82e54e01bcdc6b.png)                                                                           |
|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| 2 | Starten Sie den Installer und wählen Sie «Next».  Lesen die die Lizenzbestimmungen durch und klicken Sie danach «I Agree».  Im «Start Menu Folder»-Dialog wählen Sie «Next». | ![](media/ba7b378e70751c0ef2d4dcb9b0dea899.png) *Ignorieren Sie Versionsnummer in diesem und nachfolgenden Screenshots.*  |
| 3 | Wählen Sie die Komponenten gemäss Screenshot aus.   *Wenn Sie auf dem Server arbeiten, benötigen Sie keine lokale VM.  *                                                     | ![](media/2b71fbb7c4980344169ad43165e69695.png)![](media/5eea925e2cd5adad3736d61773b4d075.png)                            |
| 4 | Akzeptieren Sie die Standardeinstellungen und wählen Sie «Next».                                                                                                             | ![](media/fbd94b65f10c791b49e7bb818ebe40e3.png)                                                                           |
| 5 | Warten Sie, bis die Installation abgeschlossen ist.                                                                                                                          | ![](media/e616e42e2e25177cdc22b15944d49bb9.png)                                                                           |
| 6 | Wählen Sie «No» und klicken Sie auf «Next».                                                                                                                                  | ![](media/713478c99be498bfda4728299fe323a7.png)                                                                           |
| 7 | Wählen Sie «Start GNS3» ab (uncheck) und schliessen Sie die Installation mit «Finish» ab.                                                                                    | ![](media/4f2b307ca6134a5c1c795d734a591b00.png)                                                                           |

## 3.2. OpenVPN

Die Verbindung zum GNS3 Server erfolgt über ein Layer 2 OpenVPN Tunnel.

Führen Sie diese Schritte aus, sofern Sie den OpenVPN Community Client noch nicht installiert haben.

| 1 | Laden Sie den OpenVPN Community Client auf  <https://openvpn.net/community-downloads/> herunter.                                                                                                                                                                                                                         | ![](media/643edf407ae9a374f0119ffc059bb1f2.png)  |
|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| 2 | Führen Sie die Installation mit den Standarteinstellungen durch.                                                                                                                                                                                                                                                         |                                                  |
| 3 | Von der Lehrperson erhalten Sie eine **ovpn** Datei.  Jede Gruppe hat jeweils **eine** *ovpn* Datei. Alle Gruppenmitglieder haben **dieselbe** Datei.  Wenn Sie die Datei noch nicht haben, melden Sie sich bei der Lehrperson.                                                                                          |                                                  |
| 4 | Erstellen Sie im Pfad  C:\\Users\\*YOUR USERNAME*\\OpenVPN\\config  einen Ordner «gns3lab» und legen Sie die *ovpn* Datei dort ab.  (Der Dateiname ist je nach Server leicht unterschiedlich.)                                                                                                                           | ![](media/d251687d8620426c0e13e53aa805e898.png)  |
| 5 | In der Taskleiste finden Sie das OpenVPN  Symbol (![](media/bff87c4dbcc1aac6a8a55b4627fc6800.png)). Klicken Sie mit der rechten Maustaste auf das Symbol und wählen Sie «Verbinden».  *Hinweis: Wenn Sie bereits OpenVPN auf Ihrem Computer installiert haben, hat es möglicherweise mehrere Verbindungen zur Auswahl.*  | ![](media/2f5c656186c63946ced240b44ba5a85a.png)  |
| 6 | Ein Statusfenster mit einem Log öffnet sich. Sobald Sie verbunden sind, schliesst sich das Fenster.                                                                                                                                                                                                                      | ![](media/cb697e4930ae6a44e90b9af14aa0684e.png)  |
| 7 | Haben Sie sich erfolgreich verbunden, so wird das Symbol grün.                                                                                                                                                                                                                                                           | ![](media/f505e6f0f0ab4cc5b0578135b88e4dfe.png)  |

## 3.3. Verbindungstest

Stellen Sie mit einem **ping** sich sicher, dass Sie den Server erreichen können:

![](media/467b167e3b28df1a3bda6b9d6e07a24a.png)

## 3.4. GNS3 GUI

Bevor Sie die GNS3 GUI starten, stellen Sie sicher, dass Sie per OpenVPN mit dem Server verbunden sind.

| 1 | Starten Sie GNS3.                                                                              | ![](media/89e2acfd0ba569dd9ad35b24719979c8.png) |
|---|------------------------------------------------------------------------------------------------|-------------------------------------------------|
| 2 | Wählen Sie *Edit* *Preferences* und dann *Server*                                              |                                                 |
| 3 | Füllen Sie die Felder wie im Screenshot aus und klicken Sie anschliessen auf «OK».             | ![](media/39b5634f949316c5248c9218f8131974.png) |
| 4 | Schliessen Sie GNS3 und öffnen Sie es erneut, damit die Einstellungen korrekt geladen werden.  |                                                 |
| 5 | Wenn alles erfolgreich war, sehen Sie jetzt das «Project»-Fenster.                             | ![](media/a2a3cdcd03eca9a2e4e014fd7836429a.png) |

**Wenn sich alle Gruppenmitglieder erfolgreich mit dem Server verbunden haben, melden Sie sich bei der Lehrperson.   
**

# 4. Übung 1 – Statisches Routing

## 4.1. Lernziele

-   Auffrischen der IPv4 Routing Kenntnisse

-   Erste Erfahrungen mit MikroTik Router OS und GNS3 sammeln

## 4.2. Aufgabe

Ihr Computer ist über ein Layer 2 VPN mit dem GNS3 Server verbunden. Das hat den Vorteil, dass Sie ohne Routing Ihr virtuelles Netzwerk im GNS3 mit der Aussenwelt, beziehungsweise Ihrem Computer verbinden können. Dafür ist das Subnetz **192.168.23.0/24** vorgesehen. **192.168.23.1/24** ist die Adresse des GNS3 Servers. Ein DHCP Server ist aktiv und hat das IP-Pool **192.168.23.129 – 200.**

**Ziel der Aufgabe:** Von Ihrem Computer den Router R2 anzupingen (Mit R1 als Layer 3 HOP).

| **Subnetz A** | **192.168.23.0/24**                                                                                                      |
|---------------|--------------------------------------------------------------------------------------------------------------------------|
| **Subnetz B** | **192.168.X.0/24** *Nehmen Sie für X Ihre Gruppennummer plus 50. Beispiel: Gruppennummer 5 + 50 = 55  192.168.55.0/24.*  |
| **Router OS** | **MikroTik CHR V6.\***                                                                                                   |

![](media/753f05126a4874518c0ba5af0ca9a00c.png)

Abbildung 1: Netzwerktopologie Übung 1

## 4.3. Hilfreiche Befehle und Tipps

-   Benutzername: **admin**, Passwort ist keines gesetzt

-   Auf dem Interface *ether1* ist standardmässig ein IPv4 DHCP Client aktiv.

-   Mit einem rechten Mausklick auf den Router können Sie im Kontextmenü «Console» auswählen, um die Konsole des Routers zu öffnen und ihn zu konfigurieren.

-   Wählen Sie bei der Cloud **nur** das interface br0.

-   Mikrotik Befehle:

    -   /ip address add address=X.X.X.X/X interface=etherX

    -   /ip route ?

-   Vergessen Sie nicht die Route zu Subnetz B auch auf ihrem Laptop einzurichten!

-   Können Sie auch via Webbrowser auf das Webinterface von Router R2 zugreifen?

![](media/cdb6151cd1ec80615aa91d311a087b4b.png)

Abbildung 2: Übung 1 in Action: Auf den beiden Router wurden die IP-Adressen über das Subnetz B gesetzt. Kann R1 jetzt R2 anpingen?

# 5. Übung 2 – Bridging

## 5.1. Lernziele

-   Sie können virtuelle Bridges in MikroTik OS konfigurieren.
-   Sie wissen wie man VPCS als Testclients einsetzt.

## 5.2. Aufgabe

-   Duplizieren Sie in GNS3 Ihr funktionierendes Projekt von Übung 1.
-   Erstellen Sie auf **R2** eine virtuelle Bridge und fügen Sie Ports **ether3, ether4 und ether5** hinzu.
-   Weisen Sie der Bridge die kleinste Adresse von **Subnetz C** hinzu (Host min).
-   Geben Sie den beiden VPCS eine IP-Adresse aus **Subnetz C.**

## 5.3. Ressourcen

-   <https://wiki.mikrotik.com/wiki/Manual:Interface/Bridge>

**Ziel der Aufgabe:** Beide VPCS können R1 anpingen.

**Zusatzpunkte:** Auf R2 ist ein DHCP-Server für Subnetz C aktiv.

| **Subnetz A** | **192.168.23.0/24**                                                                                                         |
|---------------|-----------------------------------------------------------------------------------------------------------------------------|
| **Subnetz B** | **Siehe Übung 1**                                                                                                           |
| **Subnetz C** | **192.168.X.0/24** *Nehmen Sie für X Ihre Gruppennummer plus 100. Beispiel: Gruppennummer 5 + 100 = 105  192.168.105.0/24.* |
| **Router OS** | **MikroTik CHR V6.\***                                                                                                      |

## 5.4. Tipps

-   Verwenden Sie bei dieser Übung das Webinterface von MikroTik für die Konfiguration der Bridges.
-   Mit /export können Sie die aktuelle Konfiguration des Routers auslesen.
-   OSI Layer: Gehen Sie von unten nach oben vor: Beginnen Sie bei Layer 1 (Physisch), dann Layer 2 (Data Link Layer) und zum Schluss Layer 3 (Network Layer).
-   Weisen Sie die IP-Adresse für Subnetz C der neu erstellen Bridge zu.

![](media/001c60438a8e96628a1043b061dc7ed1.png)

Abbildung 3: So sollte es aussehen: Beide VPCS pingen erfolgreich R1 an.

# 6. Zusatzübung – Cisco 3745 (Freiwillig)

Viele Hersteller orientieren sich bei der Entwicklung Ihrer CLI beim Marktleader Cisco. Wenn Sie die Bedienung der Cisco CLI beherrschen, dann wird es Ihnen leicht fallen die Konfiguration anderer Hersteller nachzuvollziehen. In dieser Übung bauen Sie dieselbe Struktur auf, wie in Übung 1. Informieren Sie sich im Internet über die benötigten Befehle. Beachten Sie bei Ihrer Recherche, dass es sich um ein älteres Modell handelt und viele aktuelle Anleitungen sich auf das aktuelle OS von Cisco beziehen.

![Cisco 3745 CISCO3745 4 Slot Router – Newfangled Networks](media/0fccd834d7fc2d14d39e8ea2330fe25e.jpeg)

Abbildung 4: Der Cisco 3745 kam 2004 auf den Markt und ist seit 2012 End-Of-Support.

## 6.1. Lernziele

-   Sie kennen den Umgang mit dem klassischen Cisco IOS

-   Sie kennen grob den geschichtlichen Hintergrund und die Entwicklung der Cisco Router

## 6.2. Aufgabe

-   Bauen Sie die Übung 1 mit Cisco 3745 nach.
-   Schauen Sie sich das Datenblatt des Cisco 3745 an, suchen Sie auf der [Cisco Webseite](https://www.cisco.com/c/en/us/products/routers/branch-routers/index.html) nach einem passenden Nachfolger und vergleichen Sie. Welche neuen Funktionen haben die modernen Router? Wie viel mehr IP-Pakete können Sie verarbeiten (kpps)?

## 6.3. Ressourcen

-   <https://web.archive.org/web/20050329004408/http://www.cisco.com/en/US/products/hw/routers/ps282/products_data_sheet09186a008009203f.html>
-   <https://en.wikipedia.org/wiki/List_of_Cisco_products>
-   <https://www.cisco.com/c/en_dz/about/blog-africa/2017/8-things-you-didnt-know-about-Cisco.html>
-   <https://en.wikipedia.org/wiki/Cisco_IOS>
-   <https://www.arnnet.com.au/slideshow/561768/pictures-30-years-cisco/>

# 7. Anhang

## 7.1. Tipps für den Laborbericht im Word/LibreOffice (A4 Printformat)

| Seite         | Inhalt                                                                                                                                                                                                                                |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| jede          | Fusszeile: Modulname, Klasse, Gruppennummer, Datum, Seitenzahl                                                                                                                                                                        |
| 1             | Titelfolie Modulname, Klasse, Gruppennummer, Gruppenmitglieder, Thema (z.B. «GNS Einführung»)                                                                                                                                         |
| 2             | Inhaltsverzeichnis                                                                                                                                                                                                                    |
| 3 ff          | Beispiel für die Strukturierung (X ist ein Platzhalter): X. Übung 1 – Statisches Routing X.1 Aufgabenstellung X.2 Ziel(e) X.3 Vorgehen (Schritt für Schritt Anleitung) X.4 Herausforderungen (Schwierigkeiten) X.5 Fazit / Nachweise  |
| Letzte – 1 ff | Persönliche Fazits der einzelnen Gruppenmitglieder                                                                                                                                                                                    |
| Letzte        | Evt. Quellen, Abbildungsverzeichnis,                                                                                                                                                                                                  |

## 7.2. Tipps für den Laborbericht in Markdown

-   Erstellen Sie für jede Übung ein einzelnes Markdown File in einem eigenen Ordner
-   Speichern Sie alle Screenshots in zugehörigen Übungsordner
-   Erstellen Sie eine Index Markdown Datei (Startseite/Hauptseite) (auf github: README) und verlinken Sie zu den einzelnen Übungen
-   Führen Sie allgemeine Informationen (Gruppennummer, usw.) in der Hauptseite auf.

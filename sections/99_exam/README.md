# Leistungsbeurteilung LB1

- Art: Schriftlich (Papier) oder Ecolm
- Frageart: Multiple-Choice, Kurzantworten
- Durchführung: Zweitletzte Woche
- Dauer: ~45min
- 80 % Richtig = Note 6

# Allgemeine Bedingungen
 - Elektronische Geräte jeglicher Art sind während der Prüfung nicht erlaubt (ausgenommen Laptop bei Ecolm Prüfungen) und müssen vom Tisch entfernt werden. 
 - Während der Prüfung darf ein selbstgeschriebener Spick (siehe Zulassungsbedingungen) verwendet werden. 

# Zulassungsbedingungen
 - Selbstgeschriebene Zusammenfassung
   - Ausgedruckt: Max. 2 x A4 Papier (= 4 Seiten)
   - Hangeschrieben: Max. 4 x A4 Papier (= 8 Seiten)
 - Individuelle Zusammenfassung
   - Keine Kopierten Zusammenfassungen von anderen Lernenden
 - Keine "Collagen": Aus Unterlagen 1:1 übernommenen Text- oder Seitenausschnitte sind nicht zugelassen. 
   - Kopierte Grafiken / Schemas sind erlaubt.
 - Mindestumfang: Wichtigste Punkte jedes Prüfungsrelevanten Dokumentes
 - Zusammenfassung muss mit der Prüfung abgegeben werden.
 - Nicht erfüllen der Zulassungsbedingungen: 1 Note Abzug

# Prüfungsstoff
 - [Einführung GNS3 Labor](../01_intro/README.md)
 - [VLAN](../02_vlan/README.md)
 - [WLAN](../03_wlan/README.md)
 - [SNMP](../04_snmp/README.md)
 - Alle Ressourcen im [BSCW](https://bscw.tbz.ch/bscw/bscw.cgi/32878156):
   - compendio_145_2012_kap3_dokumentation.pdf
   - compendio_145_2012_kap4_instrumente_zur_ueberwachung.pdf 
   - compendio_145_2015_kap6_funknetzwerke.pdf
   - m145 Beispiele Netzwerkpläne v1.0.pdf
   - NW_Grundl_06_Kap_15_netzwerkueberwachung.pdf
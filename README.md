# M145 Netzwerk betreiben und erweitern

Aufbauend auf den vorhergehenden Modulen 117 und 129 erlernen Sie im Modul 145 die Überwachung der Performance und Verfügbarkeit von Netzwerken, den Einsatz von VLAN zur Netzwerksegmentierung und die Grundlagen des weltweit verbreiteten WLANs.

Die aktuellen Handlungsziele und -kompetenzen finden Sie hier: [ICT-Berufsbildung Schweiz Modul 145](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=145&noheader=1)

# Themen

| N | Thema |
| - | ----- |
| 1 | [Intro](sections/01_intro/README.md) |
| 2 | [VLAN](sections/02_vlan/README.md) |
| 3.1 | [WLAN Messungen](sections/03_wlan/01_messungen/README.md) |
| 3.2 | [WLAN Labor](sections/03_wlan/02_labor/README.md) |
| 4 | [SNMP](sections/04_snmp/README.md) |
| 5 | [VPN](sections/05_vpn/README.md) |

# Leistungsbeurteilung

 - LB1: Multiple-Choice-Prüfung mit Zusammenfassung (Pflicht!) über alle Themenbereiche hinweg. Weitere Informationen finden Sie [hier](sections/99_exam/README.md). 
 - LB2 - 5: Laborberichte zu Intro, VLAN, WLAN und SNMP geben je eine Note. Die Bewertungskriterien finden sie [hier](sections/00_evaluation/README.md). 
 
 Jeder Laborbericht und die Prüfung ergeben je eine Note.
 
 # Unterlagen
  - Gitrepository [alptbz/m145](https://gitlab.com/alptbz/m145/)
  - Gitbook [alptbz.gitlab.io/m145](https://alptbz.gitlab.io/m145/)
  - [BSCW](https://bscw.tbz.ch/bscw/bscw.cgi/32878156)
  - Präsentationen im Teams (individuell)